package com.qf.game.application.entity;

import javax.websocket.Session;
import java.util.List;

/**
 * 房间类
 */
public class Room {

    private int rid;//房间号
    private Player player1;//玩家1
    private Player player2;//玩家2
    private List<Player> subscribe;//第三方观察者
    private int status;//0 - 待开始 1 - 正在进行游戏
    private String info;//房间的信息
    private String pass;//房间的密码
    //棋盘对象
    private Gobang gobang = new Gobang();

    /**
     * 根据自己的session对象 获取对手的玩家信息
     * @param session
     * @return
     */
    public Player getOtherPlayer(Session session) {
        if(player1 != null && player1.getSession() == session){
            return player2;
        } else {
            return player1;
        }
    }

    /**
     * 根据session获得当前的玩家对象
     * @param session
     * @return
     */
    public Player getPlayer(Session session){
        if(player1 != null && player1.getSession() == session){
            return player1;
        } else {
            return player2;
        }
    }

    public int getRid() {
        return rid;
    }

    public void setRid(int rid) {
        this.rid = rid;
    }

    public Player getPlayer1() {
        return player1;
    }

    public void setPlayer1(Player player1) {
        this.player1 = player1;
    }

    public Player getPlayer2() {
        return player2;
    }

    public void setPlayer2(Player player2) {
        this.player2 = player2;
    }

    public List<Player> getSubscribe() {
        return subscribe;
    }

    public void setSubscribe(List<Player> subscribe) {
        this.subscribe = subscribe;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public Gobang getGobang() {
        return gobang;
    }

    public void setGobang(Gobang gobang) {
        this.gobang = gobang;
    }
}
