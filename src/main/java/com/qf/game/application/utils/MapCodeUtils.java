package com.qf.game.application.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 管理忘记密码的验证码 - 工具类
 */
public class MapCodeUtils {

    private static Map<String, List> codeMap = new ConcurrentHashMap<>();//线程安全的Map

    /**
     * 设置验证码
     */
    public static void putCode(String key, String code){
        //将验证码包装成List集合
        List list = new ArrayList();
        list.add(code);//验证码
        list.add(System.currentTimeMillis());//当前时间 毫秒值

        codeMap.put(key, list);
    }

    /**
     * 获取验证码
     * @return
     */
    public static String getCode(String key){
        //判断key是否存在
        if (codeMap.containsKey(key)) {
            //验证码存在
            List valList = codeMap.get(key);
            //获取验证码
            String code = (String) valList.get(0);
            //当初设置验证码的时间
            long setTime = (long) valList.get(1);

            //判断时效性
            //当前时间
            long nowTime = System.currentTimeMillis();

            //是否过期
            if(nowTime - setTime > 5 * 60 * 1000){
                //移除当前过期的验证码
                codeMap.remove(key);
                //验证码时效性已过
                return null;
            }

            return code;
        }

        //当前 key 对应的验证码不存在
        return null;
    }

    /**
     * 移除验证码
     * @param key
     */
    public static void removeCode(String key){
        codeMap.remove(key);
    }
}
