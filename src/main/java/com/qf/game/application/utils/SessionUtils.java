package com.qf.game.application.utils;

import com.alibaba.fastjson.JSON;

import javax.websocket.Session;
import java.io.IOException;

/**
 * 发送消息给客户端的工具类
 */
public class SessionUtils {

    public static void sendMsg(Session session, Object msgObj){
        try {
            if (msgObj instanceof String) {
                session.getBasicRemote().sendText((String) msgObj);
            } else {
                session.getBasicRemote().sendText(JSON.toJSONString(msgObj));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
