package com.qf.game.application.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.game.application.entity.RechargeOrder;

public interface RechargeOrderDao extends BaseMapper<RechargeOrder> {
}
