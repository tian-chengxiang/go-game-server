package com.qf.game.application.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.game.application.entity.User;

/**
 * 操作用户表的方法
 */
public interface UserDao extends BaseMapper<User> {
}
