package com.qf.game.application.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.game.application.entity.HappyBeanHistory;

public interface HappyBeanHistoryDao extends BaseMapper<HappyBeanHistory> {
}
