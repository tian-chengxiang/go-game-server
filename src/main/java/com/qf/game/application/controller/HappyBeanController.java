package com.qf.game.application.controller;

import com.qf.game.application.entity.HappyBeanHistory;
import com.qf.game.application.service.IHappyBeanHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin
public class HappyBeanController {

    @Autowired
    private IHappyBeanHistoryService happyBeanHistoryService;

    /**
     * 根据用户id，查询欢乐豆流水记录列表
     * @param uid
     * @return
     */
    @RequestMapping("/happybean/list")
    public List<HappyBeanHistory> queryListByUid(Integer uid){
        return happyBeanHistoryService.queryHistoryByUid(uid);
    }
}
