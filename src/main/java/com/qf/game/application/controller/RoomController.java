package com.qf.game.application.controller;

import com.qf.game.application.entity.Room;
import com.qf.game.application.entity.RoomVo;
import com.qf.game.application.utils.RoomManagerUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 房间管理接口
 */
@RestController
@CrossOrigin
public class RoomController {

    /**
     * 返回房间列表
     * @return
     */
    @RequestMapping("/room/list")
    public List<RoomVo> roomList(){
        List<Room> rooms = RoomManagerUtils.getRooms();

        //Stream流 + Lambda表达式

        //将room的集合 转换成 roomvo的集合
        List<RoomVo> roomVos = rooms
                .stream()
                .map(room -> {
            //将room对象 转换成 roomvo
            RoomVo roomVo = new RoomVo();
            roomVo.setRid(room.getRid());
            roomVo.setNickName(room.getPlayer1().getUser().getNickname());
            roomVo.setInfo(room.getInfo());
            roomVo.setIsPass(StringUtils.isEmpty(room.getPass()) ? 0 : 1);
            roomVo.setStatus(room.getStatus());
            return roomVo;
        }).collect(Collectors.toList());

        return roomVos;
    }
}
