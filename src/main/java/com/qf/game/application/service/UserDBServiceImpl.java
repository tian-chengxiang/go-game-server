package com.qf.game.application.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.qf.game.application.dao.UserDao;
import com.qf.game.application.entity.User;
import com.qf.game.application.utils.MapCodeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.Date;

/**
 * 数据库版本的用户操作
 * alt + 回车 - 万能键
 */
@Service
@Primary
public class UserDBServiceImpl implements IUserService {

    @Autowired
    private UserDao userDao;

    @Autowired
    private JavaMailSender javaMailSender;

    /**
     * 用户注册
     * @param user
     * @return
     */
    @Override
    public int register(User user) {

        //验证用户名是否存在
        //select count(*) from user where username = ?
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("username", user.getUsername());
        int count = userDao.selectCount(queryWrapper);

        if (count > 0) {
            //用户名存在
            return -1;
        }

        //验证邮箱是否存在
        QueryWrapper queryWrapper2 = new QueryWrapper();
        queryWrapper2.eq("email", user.getEmail());
        int ecount = userDao.selectCount(queryWrapper2);

        if (ecount > 0) {
            //邮箱存在
            return -2;
        }

        //将用户对象保存到数据库表中
        return userDao.insert(user);
    }

    /**
     * 用户登录
     * @param username
     * @param password
     * @return
     */
    @Override
    public User login(String username, String password) {

        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("username", username);
        queryWrapper.eq("password", password);
        User user = userDao.selectOne(queryWrapper);
        return user;
    }

    /**
     * 发送验证码邮件 根据 用户名
     * @param username
     * @return
     *  -1 用户名不存在 无法找回密码
     *  -2 邮件发送失败
     *  1 邮件发送成功
     */
    @Override
    public int sendEmailByUserName(String username) {

        //判断用户名是否存在
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("username", username);
        int count = userDao.selectCount(queryWrapper);
        if (count == 0) {
            //用户名不存在
            return -1;
        }

        //通过用户名查询邮箱
        QueryWrapper queryWrapper2 = new QueryWrapper();
        queryWrapper2.eq("username", username);
        User user = userDao.selectOne(queryWrapper2);
        //获取当前用户的邮箱
        String email = user.getEmail();

        //生成一个验证码 4   [1000 ~ 10000) 1000 ~ 9999
        int code = (int) (Math.random() * 9000 + 1000);

        //将验证码保存起来 - Map集合中 username - (code,时间)
        MapCodeUtils.putCode(username, code + "");

        //给当前邮箱发送邮件
        //mimeMessage邮件对象
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        try {
            //将邮件对象修饰一下，方便设置  参数2 - 表示是否支持附件发送
            MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);
            //设置邮件的相关属性
            //邮件标题
            mimeMessageHelper.setSubject("锋迷网络密码找回邮件");
            //邮件的发送方
            mimeMessageHelper.setFrom("verygoodwlk@sina.cn");
            //邮件的接收方
            mimeMessageHelper.setTo(email);//设置普通接收方
//            mimeMessageHelper.setCc();//设置抄送方
//            mimeMessageHelper.setBcc();//设置密送方
            //邮件的内容 参数二代表是否支持Html文本
            mimeMessageHelper.setText(
                    "<h1>找回密码</h1>找回密码的验证码：" + code + "，如果非本人操作，请忽略", true);
            //邮件的发送时间
            mimeMessageHelper.setSentDate(new Date());
            //添加附件
//            mimeMessageHelper.addAttachment("我的附件", new File("C:\\Users\\Ken\\Pictures\\Saved Pictures\\为了部落.jpg"));

            //发送邮件
            javaMailSender.send(mimeMessage);

            return 1;
        } catch (MessagingException e) {
            e.printStackTrace();
        }

        return -2;
    }

    /**
     * 根据用户名 重置 密码
     * @param username
     * @param newPassword
     * @return
     */
    @Override
    public int updatePassword(String username, String newPassword) {

        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("username", username);
        User user = userDao.selectOne(queryWrapper);

        //重新设置用户密码
        user.setPassword(newPassword);

        //修改
        int result = userDao.updateById(user);
        return result;
    }

    /**
     * 根据用户名 修改头像
     * @param username
     * @param header
     * @return
     */
    @Override
    public int updateHeader(String username, String header) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("username", username);
        User user = userDao.selectOne(queryWrapper);

        //重新设置用户头像
        user.setHeader(header);

        //修改
        int result = userDao.updateById(user);
        return result;
    }

    /**
     * 根据用户id 新增欢乐豆
     * @param uid
     * @param happyBean
     * @return 最新的欢乐豆
     */
    @Override
    public synchronized int addHappyBean(Integer uid, Integer happyBean) {
        //查询用户信息
        User user = userDao.selectById(uid);
        //设置欢乐豆
        user.setHappyBean(user.getHappyBean() + happyBean);
        //修改用户
        userDao.updateById(user);
        return user.getHappyBean();
    }

}
