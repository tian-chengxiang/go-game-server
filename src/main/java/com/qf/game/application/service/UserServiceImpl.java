package com.qf.game.application.service;

import com.qf.game.application.entity.User;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements IUserService {

    /**
     * 临时的注册容器
     */
    private List<User> userList = new ArrayList<>();

    /**
     * 模拟注册
     * @param user
     * @return
     */
    @Override
    public int register(User user) {
        //循环List
        for (User user1 : userList) {
            //判断当前用户名是否存在
            if (user1.getUsername().equals(user.getUsername())) {
                //说明当前注册的用户名 和 集合中的某个用户名 相同
                return -1;
            }

            //判断当前邮箱是否注册
            if (user1.getEmail().equals(user.getEmail())) {
                //说明邮箱已经被注册
                return -2;
            }
        }

        //将用户添加到容器中
        userList.add(user);
        //注册成功
        return 1;
    }

    /**
     * 登录逻辑
     * @param username
     * @param password
     * @return
     */
    @Override
    public User login(String username, String password) {

        //循环每个用户，判断用户名和密码
        for (User user : userList) {
            if (user.getUsername().equals(username)
                    && user.getPassword().equals(password)) {
                //已经找到用户名和密码匹配的用户
                return user;
            }
        }
        //登录失败
        return null;
    }

    @Override
    public int sendEmailByUserName(String username) {
        return 0;
    }

    @Override
    public int updatePassword(String username, String newPassword) {
        return 0;
    }

    @Override
    public int updateHeader(String username, String header) {
        return 0;
    }

    @Override
    public int addHappyBean(Integer uid, Integer happyBean) {
        return 0;
    }
}
