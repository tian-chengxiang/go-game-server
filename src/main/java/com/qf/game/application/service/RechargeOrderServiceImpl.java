package com.qf.game.application.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.qf.game.application.dao.RechargeOrderDao;
import com.qf.game.application.entity.RechargeOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 充值订单业务层的实现类
 */
@Service
public class RechargeOrderServiceImpl implements IRechargeOrderService{

    @Autowired
    private RechargeOrderDao rechargeOrderDao;

    /**
     * 下单
     * @param rechargeOrder
     * @return
     */
    @Override
    public int insertOrder(RechargeOrder rechargeOrder) {
        //保存订单记录
        return rechargeOrderDao.insert(rechargeOrder);
    }

    /**
     * 根据用户id 查询订单列表
     * @param uid
     * @return
     */
    @Override
    public List<RechargeOrder> queryRechargeOrderByUid(Integer uid) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("u_id", uid);
        queryWrapper.orderByDesc("create_time");//按照创建时间降序
        return rechargeOrderDao.selectList(queryWrapper);
    }

    /**
     * 根据订单号 修改订单状态
     * @param orderId
     * @param status
     * @return
     */
    @Override
    public int updateOrderStatus(String orderId, Integer status) {
        RechargeOrder rechargeOrder = rechargeOrderDao.selectById(orderId);
        rechargeOrder.setStatus(status);
        return rechargeOrderDao.updateById(rechargeOrder);
    }

    /**
     * 根据订单id 查询订单详细信息
     * @param orderId
     * @return
     */
    @Override
    public RechargeOrder getByOrderId(String orderId) {
        return rechargeOrderDao.selectById(orderId);
    }

    /**
     * 通过订单id 进行支付宝支付
     * @param orderId
     * @return
     */
    @Override
    public String alipayOrder(String orderId) {
        return null;
    }
}
